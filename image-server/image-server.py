# Copyright (c) Arm Ltd 2020
# Licensed under the MIT license. See LICENSE file in the project root for full license information.
import json
import os

html = """<!DOCTYPE html>
<html>
<head>
  <title>Demo: Annotated Image</title>
 <style type="text/css">

  #carbox, #personbox {
    position: relative;
    display: inline-block;
  }

  #carbox {
    padding: 10px ;
    border: 3px solid #00ff02 ;
    width: 30px;
  }
  #personbox {
    padding: 10px ;
    border: 3px solid #fe0100 ;
    width: 50px;
  }
  </style>
<script>
setInterval(function() {
    var myImageElement = document.getElementById('myImage');
    myImageElement.src = '/images/read?rand=' + Math.random() + ';base64;
}, <SLEEP_TIME>);
</script>
</head>
<body>
    <div id="imagedata">
    <img id="myImage" src="/image"/>
    </div>
       <div id="carbox">
       Car
    </div>
    <div id="personbox">
      Person
    </div>
</body>
</html>
"""

def handle(req):
    try:
        path = os.getenv("Http_Path")

        # Extract sleep time from querystring
        if path.count("/") is 1:
            filled_template = html.replace("<SLEEP_TIME>", str(int(path[1:]) * 1000))
            return html
        elif path == "/images/read":
            with open("img.txt", "r") as img_file:
                return img_file.read()
        # write request for annotated image
        elif path == "/images/write":
            parsed = json.loads(req)
            with open("img.txt", "w") as img_file:
                n = img_file.write(parsed["img"])

    except Exception as e:
        return ('{\"Exception\": "' + str(e) + '"}')
